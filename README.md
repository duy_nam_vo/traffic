# README #

## How do I get set up? ##

### For the front end ###
 
 This project is using parcel as a bundler, please ensure that it is installed https://www.npmjs.com/package/parcel

* cd TrafficFrontEnd
* npm i
* npm start

### For the back end ###

This project is using the 3.1 version of .netcore Sdk (https://dotnet.microsoft.com/download)

* cd TrafficApi
* dotnet tool install --global dotnet-ef --version 3.1.0 (install the dotnet-ef tool, you only to perform it once)
* dotnet ef database update (To use the migration script, you only need to perform it once)
* dotnet run

### Database configuration ###

The project has been tested on SQL SERVER Express v14