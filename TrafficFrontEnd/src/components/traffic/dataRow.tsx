import React,{useMemo} from 'react';
import { TrafficData } from '../../utils/types';

interface Props {
  data : TrafficData
}

const DataRow = ({data}:Props) => {
  const {capture, vehicle, image} = data;
  const {acceleration,imageID,latitude,
    longitude,speed,time,vehicleID} = capture;
  const {xCoordinate,yCoordinate} = image;
  const {type} = vehicle;
  const child = useMemo(()=> <tr>
    <td>{imageID}</td>
    <td>{vehicleID}</td>
    <td>{time}</td>
    <td>{type}</td>
    <td>{latitude}</td>
    <td>{longitude}</td>
    <td>{speed}</td>
    <td>{acceleration}</td>
    <td>{xCoordinate}</td>
    <td>{yCoordinate}</td>
  </tr>,[acceleration,imageID,latitude,longitude,speed,time,vehicleID,xCoordinate,yCoordinate]);
  return (
    child
  );
  
}

export default DataRow;