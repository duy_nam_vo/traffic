import React, {useState,useEffect} from 'react';
import { TrafficData } from '../../utils/types';
import DataRow from './dataRow';
enum State {
  loading,
  loaded
}
const Traffic = ():JSX.Element => {
  const [state,setState] = useState<number>(State.loading);
  const [data,setData] = useState<TrafficData[]>();
  const fetchData = async () => {
    const response = await fetch(`${process.env.TRAFFIC_ENDPOINT}/Traffic`)
                        .then(response => response.json())
                        .catch(error => console.error(error));
    setData(response);
    setState(State.loaded);
  }

  useEffect(()=>{
    fetchData();
  },[]);
  switch(state){
    default:
    case State.loading:
      return <div className="h3" style={{textAlign:"center"}}> It is loading</div>;
   
    case State.loaded:
      return (<div style={{overflowX:"auto"}}>
      <table className="table table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Image ID</th>
            <th>Vehicle ID</th>
            <th>Time</th>
            <th>Vehicle Type</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th>Speed</th>
            <th>Acceleration</th>
            <th>X Coordinate (image)</th>
            <th>Y Coordinate (image)</th>
          </tr>
        </thead>
        <tbody>
          {data && data.map(trafficData=><DataRow data={trafficData} key={trafficData.capture.captureID}/>)}
        </tbody>
      </table>
    </div>);
  }
}

export default Traffic;