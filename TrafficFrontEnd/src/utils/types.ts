interface Capture { 
  imageID:number;
  acceleration:number;
  latitude:number;
  longitude:number;
  speed:number;
  vehicleID:number;
  captureID:number;
  time:number;
}

interface Vehicle { 
  id:number;
  type:string;
}

interface Image {
  imageID:number;
  xCoordinate:number;
  yCoordinate:number;
}

export interface TrafficData {
  vehicle:Vehicle;
  capture:Capture;
  image:Image;
}