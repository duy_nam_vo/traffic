import React from "react";
import ReactDOM from "react-dom";
import './scss/app.scss';
import Traffic from './components/traffic';
import dotenv from 'dotenv';
dotenv.config();
const App = (): JSX.Element => <Traffic />
let element = document.getElementById("app");
ReactDOM.render(<App />, element);