using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrafficApi.DAL;
using TrafficApi.Models;

namespace TrafficApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrafficController : ControllerBase
    {
        private readonly TrafficContext _context;

        public TrafficController(TrafficContext context)
        {
            _context = context;
        }

        // GET: api/Traffic
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleCapture>>> GetCapture()
        {
            var capture = await _context.Captures.ToListAsync();
            var vehicle = await _context.Vehicles.ToListAsync();
            var image = await _context.Images.ToListAsync();
            var vehicleCapture = from c in capture
                                 join v in vehicle on c.VehicleID equals v.ID into vc
                                 join i in image on c.ImageID equals i.ImageID into ic
                                 from v in vc.DefaultIfEmpty()
                                 from i in ic.DefaultIfEmpty()
                                 select new VehicleCapture {capture =c, vehicle = v,image = i};
            return vehicleCapture.ToList();
        }
    }
}
