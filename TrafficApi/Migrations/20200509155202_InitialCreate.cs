﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrafficApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    ImageID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: true),
                    XCoordinate = table.Column<double>(nullable: false),
                    YCoordinate = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.ImageID);
                });

            migrationBuilder.CreateTable(
                name: "Vehicle",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Weight = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Capture",
                columns: table => new
                {
                    CaptureID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Time = table.Column<double>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Speed = table.Column<double>(nullable: false),
                    Acceleration = table.Column<double>(nullable: false),
                    VehicleID = table.Column<int>(nullable: false),
                    ImageID = table.Column<int>(nullable: false),
                    VehicleID1 = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Capture", x => x.CaptureID);
                    table.ForeignKey(
                        name: "FK_Capture_Image_ImageID",
                        column: x => x.ImageID,
                        principalTable: "Image",
                        principalColumn: "ImageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Capture_Vehicle_VehicleID1",
                        column: x => x.VehicleID1,
                        principalTable: "Vehicle",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Image",
                columns: new[] { "ImageID", "Url", "XCoordinate", "YCoordinate" },
                values: new object[,]
                {
                    { 35075, "imageUrl", 536.0326, 537.37570000000005 },
                    { 35076, "imageUrl", 570.40290000000005, 541.9049 },
                    { 35077, "imageUrl", 606.14739999999995, 547.14059999999995 },
                    { 35078, "imageUrl", 643.63009999999997, 553.03420000000006 },
                    { 35079, "imageUrl", 683.20780000000002, 559.53279999999995 }
                });

            migrationBuilder.InsertData(
                table: "Vehicle",
                columns: new[] { "ID", "Type", "Weight" },
                values: new object[] { 13043L, "Car", 100.2 });

            migrationBuilder.InsertData(
                table: "Capture",
                columns: new[] { "CaptureID", "Acceleration", "ImageID", "Latitude", "Longitude", "Speed", "Time", "VehicleID", "VehicleID1" },
                values: new object[,]
                {
                    { 1L, 5.5038479999999996, 35075, -27.559899999999999, 153.0813, 8.4702029999999997, 0.20000000000000001, 13043, null },
                    { 2L, 5.416137, 35076, -27.559899999999999, 153.0813, 9.0118170000000006, 0.20000000000000001, 13043, null },
                    { 3L, 5.0914289999999998, 35077, -27.559899999999999, 153.0813, 9.5209600000000005, 0.20000000000000001, 13043, null },
                    { 4L, 4.6128299999999998, 35078, -27.559899999999999, 153.0813, 9.9822430000000004, 0.20000000000000001, 13043, null },
                    { 5L, 4.0377419999999997, 35079, -27.559899999999999, 153.0813, 10.38602, 0.20000000000000001, 13043, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Capture_ImageID",
                table: "Capture",
                column: "ImageID");

            migrationBuilder.CreateIndex(
                name: "IX_Capture_VehicleID1",
                table: "Capture",
                column: "VehicleID1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Capture");

            migrationBuilder.DropTable(
                name: "Image");

            migrationBuilder.DropTable(
                name: "Vehicle");
        }
    }
}
