using TrafficApi.Models;
using Microsoft.EntityFrameworkCore;

namespace TrafficApi.DAL
{
    public class TrafficContext : DbContext
    {
        public TrafficContext(DbContextOptions<TrafficContext> options)
          : base(options)
        {
        }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Capture> Captures { get; set; }
        public DbSet<Image> Images { get; set; }
         protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vehicle>().ToTable("Vehicle");
            modelBuilder.Entity<Capture>().ToTable("Capture");
            modelBuilder.Entity<Image>().ToTable("Image");
            modelBuilder.Entity<Vehicle>().HasData(new Vehicle{ID=13043, Type="Car",Weight=100.2});
            modelBuilder.Entity<Image>().HasData(new Image{ImageID=35075,Url="imageUrl",XCoordinate=536.0326, YCoordinate=537.3757});
            modelBuilder.Entity<Image>().HasData(new Image{ImageID=35076,Url="imageUrl",XCoordinate=570.4029, YCoordinate=541.9049});
            modelBuilder.Entity<Image>().HasData(new Image{ImageID=35077,Url="imageUrl",XCoordinate=606.1474, YCoordinate=547.1406});
            modelBuilder.Entity<Image>().HasData(new Image{ImageID=35078,Url="imageUrl",XCoordinate=643.6301, YCoordinate=553.0342});
            modelBuilder.Entity<Image>().HasData(new Image{ImageID=35079,Url="imageUrl",XCoordinate=683.2078, YCoordinate=559.5328});
            modelBuilder.Entity<Capture>().HasData(new Capture{CaptureID=1,ImageID=35075,VehicleID=13043,Time=0.2,Latitude=-27.5599, Longitude=153.0813, Speed=8.470203,Acceleration=5.503848});
            modelBuilder.Entity<Capture>().HasData(new Capture{CaptureID=2,ImageID=35076,VehicleID=13043,Time=0.2,Latitude=-27.5599, Longitude=153.0813, Speed=9.011817,Acceleration=5.416137});
            modelBuilder.Entity<Capture>().HasData(new Capture{CaptureID=3,ImageID=35077,VehicleID=13043,Time=0.2,Latitude=-27.5599, Longitude=153.0813, Speed=9.52096,Acceleration=5.091429});
            modelBuilder.Entity<Capture>().HasData(new Capture{CaptureID=4,ImageID=35078,VehicleID=13043,Time=0.2,Latitude=-27.5599, Longitude=153.0813, Speed=9.982243,Acceleration=4.61283});
            modelBuilder.Entity<Capture>().HasData(new Capture{CaptureID=5,ImageID=35079,VehicleID=13043,Time=0.2,Latitude=-27.5599, Longitude=153.0813, Speed=10.38602,Acceleration=4.037742});
        }
    }
}