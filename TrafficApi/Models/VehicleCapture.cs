namespace TrafficApi.Models {
  public class VehicleCapture{
    public Vehicle vehicle {get;set;}
    public Capture capture {get;set;}
    public Image image{get;set;}
  }
}