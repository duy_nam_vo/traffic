using System;
namespace TrafficApi.Models{
  public class Image{
    public int ImageID {get;set;}
    public string Url {get;set;}
    public double XCoordinate{get;set;}
    public double YCoordinate{get;set;}
  }
}