using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficApi.Models{
  public class Vehicle{
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public long ID{get;set;}
    public string Type{get;set;}
    public double Weight{get;set;}
    public virtual ICollection<Capture> Captures {get;set;}
  }
}