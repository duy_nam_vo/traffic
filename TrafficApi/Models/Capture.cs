using System;

namespace TrafficApi.Models{
  public class Capture{
    public long CaptureID{get;set;}
    public double Time{get;set;}
    public double Latitude{get;set;}
    public double Longitude{get;set;}
    public double Speed{get;set;}
    public double Acceleration{get;set;}
    public int VehicleID{get;set;}
    public int ImageID{get;set;}
    public virtual Vehicle Vehicle{get;set;}
    public virtual Image Image{get;set;}
  }
}